/******************* 
  Patrick Goodwin
  phgoodw               
  Lab 5                         
  Lab Section: 001
  Nushrat Humaira 
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
#include "stdlib.h"
#include "string.h"
using namespace std;

int main(int argc, char const *argv[]) {
    int number[5];
    string suit[5];
    int tempVal1;
    int i, k;
    srand(time(0));
    for(i = 0; i < 5; ++i)
    {
        number[i] = rand() % 13;
        tempVal1 = rand() % 4;
        if (tempVal1 == 0) { suit[i] = "SPADES"; }
        if (tempVal1 == 1) { suit[i] = "HEARTS"; }
        if (tempVal1 == 2) { suit[i] = "DIAMONDS"; }
        if (tempVal1 == 3) { suit[i] = "CLUBS"; }
    }
    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    for(k = 0; k < 5; ++k)
    {
        if (number[k] == 1)
        {
            cout << "Ace of " << suit[k] << endl;
        }
        else if (number[k] == 11)
        {
            cout << "Jack of " << suit[k] << endl;
        }
        else if (number[k] == 12)
        {
            cout << "Queen of " << suit[k] << endl;
        }
        else if (number[k] == 13)
        {
            cout << "King of " << suit[k] << endl;
        }
        else
        {
            cout << number[k] << " of " << suit[k] << endl;
        }
    }
  return 0;
}

